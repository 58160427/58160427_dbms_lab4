CREATE VIEW STD AS
SELECT s.id, t.tname AS title,
	s.tname AS name, s.tsurname AS surname,
	m.tname AS major
FROM student AS s
LEFT JOIN ( major AS m, title AS t)
ON ( s.major_id = m.id AND s.title_id = t.tid );
