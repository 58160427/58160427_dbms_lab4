CREATE VIEW SalePerOrder AS
	SELECT
		orderNumber, SUM(quantityOrdered * priceEach) total
	FROM
		orderdetails
	GROUP by orderNumber
	ORDER BY total DESC;
